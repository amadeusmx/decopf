<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('/mensaje',function(Request $request){
    $msg = [
        'inputNombre' => $request->inputNombre ?? '',
        'inputEmail' => $request->inputEmail ?? '',
        'inputAsunto' => $request->inputAsunto ?? '',
        'inputMessage' => $request->inputMessage ?? ''
    ];
   
    \Mail::to('elvia.cisne@decoracionesparafiestas.com.mx')->send(new \App\Mail\EmailRobotSender($msg));
   
    return redirect('/');
});
