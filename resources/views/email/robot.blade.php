<!DOCTYPE html>
<html>
<head>
    <title>Mensaje de web</title>
</head>
<body>
    <h1>Nuevo mensaje</h1>
    <p>Estimada Naxhieli, soy el robot de correo electrónico y es un placer para mí comentarte que alguien o algo dejó un mensaje en tu buzón:</p>
    <p>Nombre: <strong>{{$msg['inputNombre']}}</strong></p>
    <p>Email: <strong><a href="mailto:{{$msg['inputEmail']}}">{{$msg['inputEmail']}}</a></strong></p>
    <p>Asunto: <strong>{{$msg['inputAsunto']}}</strong></p>
    <p>Mensaje <strong>{{$msg['inputMessage']}}</strong></p>
   
    <p>Ten un bonito día Naxhieli!!!</p>
</body>
</html>