<!DOCTYPE html>
<html lang="es-MX">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Decoracion, arreglo, adorno, ornato, diseño para fiesta, evento, festejo, distinción, celebración o acontecimiento" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-KE3WK6JS1Q"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'G-KE3WK6JS1Q');
	</script>
	<link href="http://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="/css/style.css" type="text/css" />
	<link rel="stylesheet" href="/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="/css/swiper.css" type="text/css" />
	<!-- Photography Specific Stylesheet -->
	<link rel="stylesheet" href="/photography.css" type="text/css" />
	<link rel="stylesheet" href="/css/photography-addons.css" type="text/css" />

	<link rel="stylesheet" href="/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="/css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="/css/fonts.css" type="text/css" />
	<link href="/css/leaflet.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="/css/colors.php?color=c85e51" type="text/css" />

	<title>Decoraciones | Arreglos | Adornos y más</title>

</head>

<body class="stretched side-header side-header-right open-header">

	<div id="wrapper" class="clearfix">

		<div class="co-name d-none d-lg-block center"><img src="/images/logo-df-nobg.png" alt="decoracionesparafiestas logo"></div>

		<header id="header" class="no-sticky dark">
			<div id="header-wrap">
				<div class="container clearfix">
					<div class="header-title d-none d-lg-block">Decoracionesparafiestas.com.mx</div>
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					
					<div id="logo" class="d-block d-lg-none d-xl-block">
						<a href="demo-photography.html" class="standard-logo" data-dark-logo="/images/logo-df-nobg.png"><img src="/images/logo-df-nobg.png" alt="decoracionesparafiestas Logo"></a>
						<a href="demo-photography.html" class="retina-logo" data-dark-logo="/images/logo-df-nobg.png"><img src="/images/logo-df-nobg.png" alt="decoracionesparafiestas Logo"></a>
					</div><!-- #logo end -->

					<nav id="primary-menu" class="nobottommargin clearfix">
						<ul>
							<li class="current"><a href="#"><div>Home</div></a></li>
							<li><a href="#" data-scrollto="#galeria"><div>Galería</div></a></li>
							<li><a href="#" data-scrollto="#bio"><div>Bio</div></a></li>
							<li><a href="#" data-scrollto="#contacto"><div>Contáctanos</div></a></li>
							<li><a href="#" data-scrollto="#mapa"><div>Mapa</div></a></li>
						</ul>
					</nav><!-- #primary-menu end -->
				</div>

			</div>

			<div id="header-trigger"><i class="icon-line-menu"></i><i class="icon-line-cross"></i></div>

		</header><!-- #header end -->

		<section id="slider" class="slider-element swiper_wrapper full-screen clearfix" data-loop="true" data-speed="1200" data-autoplay="5000">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide dark" style="background-image: url('/images/slider/IMG-20201116-WA0005.jpg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3 style="color:black;">Amor a primera vista</h3>
								<span style="color:black;">Guirnaldas, arcos, detalles de materiales varios</span>
							</div>
						</div>
					</div>
					<div class="swiper-slide dark" style="background-image: url('/images/nsite/IMG-20201115-WA0020.jpg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3 style="color:black;">Volando ligero</h3>
								<span style="color:black;">Globos con gas helio para todo evento</span>
							</div>
						</div>
					</div>
					<div class="swiper-slide dark" style="background-image: url('/images/slider/IMG-20201116-WA0004_2.jpg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3 style="color:black;">Kids sorpresa</h3>
								<span style="color:black;">Especialistas en fiestas infantiles</span>
							</div>
						</div>
					</div>
					<div class="swiper-slide dark" style="background-image: url('/images/nsite/IMG-20201115-WA0027.jpg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3 style="color:black;">Identidad en decoración</h3>
								<span style="color:black;">Mural autóctono</span>
							</div>
						</div>
					</div>
					<div class="swiper-slide dark" style="background-image: url('/images/slider/flowers2.jpeg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3>Armonía y color</h3>
								<span>Styling</span>
							</div>
						</div>
					</div>
					<div class="swiper-slide dark" style="background-image: url('/images/slider/waflowerspath.jpeg');">
						<div class="container clearfix">
							<div class="slider-text">
								<h3 style="color:black;">Naturaleza infinita</h3>
								<span style="color:black;">Centros de mesa y accesorios</span>
							</div>
						</div>
					</div>
				</div>
				<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
				<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
				<div class="slide-number"><div class="slide-number-current"></div><span></span><div class="slide-number-total"></div></div>
			</div>

			<a href="#" data-scrollto="#content" data-offset="0" class="dark one-page-arrow"><i class="icon-line-grid"></i></a>

		</section><!-- #Slider End -->

		<section id="content" class="clearfix">

			<div class="content-wrap">
				<div class="container dark clearfix" id="galeria">
					<div class="heading-block dark center noborder">
						<h3>Galería</h3>
						<span>Observa a detalle nuestros trabajos</span>
					</div>

					<div class="grid">
						<div class="grid-item" data-size="1280x853">
							<script type="application/ld+json">
								{
								"@context": "https://schema.org",
								"@type": "CreativeWork",
								"author": "decoracionesparafiestas.com.mx",
								"contentRating": "all ages",
								"image": "/images/items/IMG-20201017-WA0010.jpg",
								"name": "Cubos de frituras"
							}
							</script>
							<a href="/images/items/IMG-20201017-WA0010.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/IMG-20201017-WA0010.jpg" alt="mesa de dulces" />
								<div class="description description-grid">
									<h3>Cubos de frituras (3)</h3>
									<p>(chicharrones, papás, churritos).<br />
										2 cubos con brochetas incertadas con variedad de gomitas y bombones pequeños.<br />
										Grada de palomitas de maíz.<br />
										Todo este paquete para entre 50 y 60 personas.<br />
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/3e87633671535cddb8c8047df7101e4d.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/3e87633671535cddb8c8047df7101e4d.jpg" alt="centro de mesa" />
								<div class="description description-grid">
									<h3>Centro de mesa con peluche</h3>
									<p>
										Muñeco de peluche a elección del cliente, globos al color de preferencia
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/20200921_014118.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/20200921_014118.jpg" alt="img03" />
								<div class="description description-grid">
									<h3>Arco de globos</h3>
									<p>
										Colores de globos a elegir se coloca en cualquier lugar por contar con estructura 6 metros lineales aprox.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/d1a5105e7f7488e95dbcc643b8ec7503.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/d1a5105e7f7488e95dbcc643b8ec7503.jpg" alt="img04" />
								<div class="description description-grid">
									<h3>Carreta decorativa</h3>
									<p>Carreta decorativa
										Rústica o sobria se recomienda para resguardo de regalos, acompañada de un arco orgánico al color de su preferencia, toldo blanco.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/20201125_120300.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/20201125_120300.jpg" alt="img05" />
								<div class="description description-grid">
									<h3>Bouquet infantil</h3>
									<p>
										Se personaliza al personaje de moda o preferencia, colores a elegir, aproximadamente 90 cm de largo ideal para rincones  y bajo arcos
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/7f6674621b60be550e18aa189a79a36b.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/7f6674621b60be550e18aa189a79a36b.jpg" alt="img06" />
								<div class="description description-grid">
									<h3>Arco de flores naturales</h3>
									<p>
										Con flores duraderas de temporada a elegir, follaje, puede ser redondo, semiredondo y tradicional, ideal para bodas.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<script type="application/ld+json">
									{
									"@context": "https://schema.org",
									"@type": "CreativeWork",
									"author": "decoracionesparafiestas.com.mx",
									"contentRating": "all ages",
									"image": "/images/items/IMG-20201017-WA0018.jpg",
									"name": "Bombonera"
								}
							</script>
							<a href="/images/items/IMG-20201017-WA0018.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/IMG-20201017-WA0018.jpg" alt="img07" />
								<div class="description description-grid">
									<h3>Bombonera</h3>
									<p>Pueden ser 1 o 2 al gusto del cliente, diferentes formas de bombones y colores.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/a90419cfb6481cc381afb362869dd171.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/a90419cfb6481cc381afb362869dd171.jpg" alt="img08" />
								<div class="description description-grid">
									<h3>Carreta decorativa</h3>
									<p>
										Carreta Decorativa:
										Ideal para recepción en salones, casas, es un toque de distinción para juntas, platicas, se colocan recuerdos, etiquetas, distintivos, volantes, propaganda, cuaderno de ingreso. Ideal para bautizos, en accesorios color blanco, toldo al color deseado, flores y luces en cada pilar.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<script type="application/ld+json">
										{
										"@context": "https://schema.org",
										"@type": "CreativeWork",
										"author": "decoracionesparafiestas.com.mx",
										"contentRating": "all ages",
										"image": "/images/items/IMG-20201017-WA0024.jpg",
										"name": "Rueda de la fortuna"
									}
							</script>
							<a href="/images/items/IMG-20201017-WA0024.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/IMG-20201017-WA0024.jpg" alt="img09" />
								<div class="description description-grid">
									<h3>Rueda de la fortuna</h3>
									<p> 8 compartimentos que contienen:<br />
									paletas, <br />chocolates, <br />perlas, <br />caramelos, <br />chicles, <br />gomitas <br /> y más<br />
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/a67d79e6e547d5697f74e8cb30d8d647.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/a67d79e6e547d5697f74e8cb30d8d647.jpg" alt="img10" />
								<div class="description description-grid">
									<h3>Table styling</h3>
									<p>
										Elegante tira de flores colocadas en medio y a lo largo de mesa, flores de temporada a elección, velas, troncos, follaje, luces,  varios accesorios más, amplia gama de sobremanteles de diferentes texturas.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/20201125_122757.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/20201125_122757.jpg" alt="img11" />
								<div class="description description-grid">
									<h3>Bouquet conejo</h3>
									<p>
										Unisex, con inicial del festejado, globos al color de su elección
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/46d8416dfceb28caf2a4770b888c2bef.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/46d8416dfceb28caf2a4770b888c2bef.jpg" alt="img12" />
								<div class="description description-grid">
									<h3>Centro de mesa ligero</h3>
									<p>
										Con mariposas u otro insecto curioso como libélulas, catarinas o abejas, cualquier color casi todo de papel.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/Screenshot_20201113-210427_Gallery2.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/Screenshot_20201113-210427_Gallery2.jpg" alt="img13" />
								<div class="description description-grid">
									<h3>Centro de mesa con rosas</h3>
									<p>
										Complemento para la table styling a elegir color de las rosas, follaje de temporada
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/c5e343cde2b46e1414a497585b9a83d6.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/c5e343cde2b46e1414a497585b9a83d6.jpg" alt="img14" />
								<div class="description description-grid">
									<h3>Carreta decorativa</h3>
									<p>
										Carreta decorativa, sobria o rústica a escoger, apta para mostrar e invitar a tomar dulces a predilección de los invitados en eventos como cumpleaños, xv años,  bodas, primeras comuniones.
										Color y adornos a elección del cliente.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/8d98b78b3c28d84a93dd02433b4f824c.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/8d98b78b3c28d84a93dd02433b4f824c.jpg" alt="img15" />
								<div class="description description-grid">
									<h3>Centro de mesa 2 pisos</h3>
									<p>
										Con base de vidrio a préstamo a preferencia del cliente, flores de temporada, para mesas redondas o cuadradas
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/20201125_164927.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/20201125_164927.jpg" alt="img16" />
								<div class="description description-grid">
									<h3>Accesorio de pared</h3>
									<p>
									Se coloca de igual forma arriba de puertas y ventanales, flores de temporada a elegir, 2 follajes diferentes verdes, mide aprox. 2 metros, implementó decorativo alterno a centro de mesa principal y adornos florales laterales del mismo estilo
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/8ddb2b4b5fab1c53f0b71f4bcdcf1aee.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/8ddb2b4b5fab1c53f0b71f4bcdcf1aee.jpg" alt="img17" />
								<div class="description description-grid">
									<h3>Arco de follaje</h3>
									<p>
										Follaje verde con decorado lateral de flores de temporada, diferentes colores, ideal para interiores.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/IMG-20201026-WA00122.jpeg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/IMG-20201026-WA00122.jpeg" alt="img18" />
								<div class="description description-grid">
									<h3>Centro de mesa 4 pisos</h3>
									<p>
										A colocar flores, accesorios, velas, frutas, imagen personalizada día de muertos
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/IMG-20201017-WA0027.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/IMG-20201017-WA0027.jpg" alt="img19" />
								<div class="description description-grid">
									<h3>Mesa de dulces completa</h3>
									<p>Consta de rueda de la fortuna con surtido de dulces:<br />
										2 cubos con brochetas de gomitas y bombones.<br />
										1 Bombonera.<br />
										3 cubos grandes con frituras surtidas.<br />
										Grada para palomitas de maíz.<br />
										Muñeco de peluche con globos o arco orgánico de globos a elegir cualquiera de los 2.
										Tema de fiesta y color de accesorios a elegir.
										
									</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/20200921_014636.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/20200921_014636.jpg" alt="img20" />
								<div class="description description-grid">
									<h3>Arco infantil con osos</h3>
									<p>
										Cualquier color, cortesía 4 arreglos individuales de globos a elegir.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="/images/items/822be7ab98c31940883fcff195e3559e.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="/images/items/822be7ab98c31940883fcff195e3559e.jpg" alt="img21" />
								<div class="description description-grid">
									<h3>Table styling</h3>
									<p>
										Elegante tira de flores colocadas en medio y a lo largo de mesa, flores de temporada a elección, velas, troncos, follaje, luces,  varios accesorios más, amplia gama de sobremanteles de diferentes texturas.
										</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- /grid -->
					<div class="preview">
						<button class="action action-close"><i class="icon-line2-close"></i><span class="text-hidden">Close</span></button>
						<div class="description description-preview"></div>
					</div>
				</div>

				<div class="clear"></div>

				<div class="section dark topmargin-lg nobottommargin clearfix" style="padding: 100px 0;">
					<div class="container clearfix">
						<div class="row clearfix" id="bio">
							<div class="col-lg-4 col-md-5 offset-lg-1">
								<img src="/images/20201115_204159.jpg" alt="decoraciones-para-fiestas" style="border: 8px solid #333;">
							</div>
							<div class="col-lg-5 col-md-7 offset-lg-1">
								<!-- <div class="topmargin d-block d-md-none d-lg-none d-xl-block"></div> -->
								<div class="heading-block noborder">
									<h2 class="ls5">Decoraciones para fiestas</h2>
									<span class="t300" style="font-size: 20px; color: #DDD;">Tomando una imagen, capturando un momento, revela verdaderamente qué tan rica es la realidad...</span>
								</div>
								<p class="text-muted">Me llamo Elvia Naxhieli Cisneros Vargas
Inicié en Ixtapaluca Estado de México en 1999 como decoradora de fiestas infantiles, bodas, aniversarios, graduaciones etc., en la elaboración de 
arreglos con globos y flores naturales, actualmente abriendo camino en la CDMX, me encanta lo que realizan mis manos, acompañada de mi equipo siendo dadores de emociones ilimitadas en todas las celebraciones de nuestros clientes.</p>
								<a href="#" class="button button-circle button-light button-white" data-scrollto="#contacto">Contáctanos</a>
							</div>
						</div>
					</div>
				</div>

				<div class="clear"></div>

				<div class="section nobg footer-stick clearfix">
					<div class="container clearfix" id="contacto">
						<div class="heading-block dark center noborder">
							<h3 class="ls1">CONTACTO</h3>
							<span>Globally re-engineer transparent materials,</span>
						</div>
						<div class="row dark clearfix">
							<form class="mx-auto" action="/mensaje" method="post">
								@csrf
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="inputNombre">Nombre</label>
										<input type="text" class="form-control" id="inputNombre" name="inputNombre" placeholder="Nombre">
									</div>
									<div class="form-group col-md-6">
										<label for="inputEmail">Correo</label>
										<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="correo">
									</div>
								</div>
								<div class="form-group">
									<label for="inputAsunto">Asunto</label>
									<input type="text" class="form-control" id="inputAsunto" name="inputAsunto" placeholder="Asunto">
								</div>
								<div class="form-group">
									<label for="inputMessage">Mensaje</label>
									<textarea name="inputMessage" id="inputMessage" cols="30" class="form-control" rows="8" placeholder="Mensaje" required=""></textarea>
								</div>
								<button type="submit" class="btn btn-primary">Enviar mensaje</button>
							</form>
						</div>
					</div>
				</div>

				<div class="section nobg footer-stick clearfix">
					<div style="height:500px;" id="mapa">
					<div id="map_content" class="h-100 leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" data-lat="19.355725" data-lng="-99.080500" data-zoom="20" tabindex="0" style="position: relative; outline: none;"><div class="leaflet-pane leaflet-map-pane" style="transform: translate3d(-461px, -218px, 0px);"><div class="leaflet-pane leaflet-tile-pane"><div class="leaflet-layer " style="z-index: 1; opacity: 1;"><div class="leaflet-tile-container leaflet-zoom-animated" style="z-index: 13; transform: translate3d(839px, 352px, 0px) scale(0.5);"></div><div class="leaflet-tile-container leaflet-zoom-animated" style="z-index: 14; transform: translate3d(559px, 234px, 0px) scale(1);"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/460/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(409px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/460/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(409px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/459/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(153px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/461/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(665px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/459/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(153px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/461/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(665px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/460/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(409px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/459/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(153px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/461/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(665px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/458/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-103px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/462/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(921px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/458/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-103px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/462/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(921px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/458/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-103px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/462/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(921px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/457/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-359px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/463/911.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(1177px, 118px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/457/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-359px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/463/910.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(1177px, -138px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/457/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(-359px, 374px, 0px); opacity: 1;"><img alt="" role="presentation" src="http://tile.openstreetmap.org/11/463/912.png" class="leaflet-tile leaflet-tile-loaded" style="width: 256px; height: 256px; transform: translate3d(1177px, 374px, 0px); opacity: 1;"></div></div></div><div class="leaflet-pane leaflet-shadow-pane"><img src="https://demo.hasthemes.com/lukas-preview-v4/lukas/assets/img/leaflet/marker-shadow.png" class="leaflet-marker-shadow leaflet-zoom-animated" alt="" style="margin-left: -12px; margin-top: -41px; width: 41px; height: 41px; transform: translate3d(277050px, -6366px, 0px);"></div><div class="leaflet-pane leaflet-overlay-pane"></div><div class="leaflet-pane leaflet-marker-pane"><img src="https://demo.hasthemes.com/lukas-preview-v4/lukas/assets/img/leaflet/marker-icon.png" class="leaflet-marker-icon leaflet-zoom-animated leaflet-interactive" alt="" tabindex="0" style="margin-left: -12px; margin-top: -41px; width: 43px; height: 55px; transform: translate3d(277050px, -6366px, 0px); z-index: -6366;"></div><div class="leaflet-pane leaflet-tooltip-pane"></div><div class="leaflet-pane leaflet-popup-pane"></div><div class="leaflet-proxy leaflet-zoom-animated" style="transform: translate3d(117920px, 233332px, 0px) scale(1024);"></div></div><div class="leaflet-control-container"><div class="leaflet-top leaflet-left"></div><div class="leaflet-top leaflet-right"></div><div class="leaflet-bottom leaflet-left"></div><div class="leaflet-bottom leaflet-right"><div class="leaflet-control-zoom leaflet-bar leaflet-control"><a class="leaflet-control-zoom-in" href="#" title="Zoom in" role="button" aria-label="Zoom in">+</a><a class="leaflet-control-zoom-out" href="#" title="Zoom out" role="button" aria-label="Zoom out">−</a></div><div class="leaflet-control-attribution leaflet-control"><a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> | Map</div></div></div></div></div>
				</div>
			</div>

		</section><!-- #content end -->

		<div class="clear"></div>

		<footer id="footer" class="dark noborder clearfix" style="background-color: #282828">

			<div id="copyrights" class="nobg">

				<div class="container clearfix">

					<div class="col_full center nobottommargin topmargin-sm">
						decoracionesparafiestas.com.mx | Tu evento es nuestro arte.<br><br>
						<a href="https://facebook.com/DecoracionesparafiestasMC" target="_blank" class="social-icon inline-block si-small si-borderless si-rounded si-facebook">
							<i class="icon-facebook"></i>
							<i class="icon-facebook"></i>
						</a>
						<a href="https://twitter.com/Decoracionespa3" target="_blank" class="social-icon inline-block si-small si-borderless si-rounded si-twitter">
							<i class="icon-twitter"></i>
							<i class="icon-twitter"></i>
						</a>
						<a href="https://instagram.com/decoraciones.para.fiestasmc" target="_blank" class="social-icon inline-block si-small si-borderless si-rounded si-instagram">
							<i class="icon-instagram"></i>
							<i class="icon-instagram"></i>
						</a>
						<a href="mailto:contacto@decoracionesparafiestas.com.mx" class="social-icon inline-block si-small si-borderless si-rounded si-gplus">
							<i class="icon-envelope2"></i>
							<i class="icon-envelope2"></i>
						</a>
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<div id="gotoTop" class="icon-angle-up"></div>

	<script src="/js/jquery.js"></script>
	<script src="/js/plugins.js"></script>
	<script src="/js/photography-addons.js"></script>
	<script src="/js/leaflet.min.js"></script>

	<script src="js/functions.js"></script>

	<script>
		(function() {
			var support = { transitions: Modernizr.csstransitions },
				// transition end event name
				transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
				onEndTransition = function( el, callback ) {
					var onEndCallbackFn = function( ev ) {
						if( support.transitions ) {
							if( ev.target != this ) return;
							this.removeEventListener( transEndEventName, onEndCallbackFn );
						}
						if( callback && typeof callback === 'function' ) { callback.call(this); }
					};
					if( support.transitions ) {
						el.addEventListener( transEndEventName, onEndCallbackFn );
					}
					else {
						onEndCallbackFn();
					}
				};

			new GridFx(document.querySelector('.grid'), {
				imgPosition : {
					x : -0.5,
					y : 1
				},
				onOpenItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							var delay = Math.floor(Math.random() * 50);
							el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
							el.style.transform = 'scale3d(0.1,0.1,1)';
							el.style.opacity = 0;
						}
					});
				},
				onCloseItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
							el.style.transition = 'opacity .4s, transform .4s';
							el.style.WebkitTransform = 'scale3d(1,1,1)';
							el.style.transform = 'scale3d(1,1,1)';
							el.style.opacity = 1;

							onEndTransition(el, function() {
								el.style.transition = 'none';
								el.style.WebkitTransform = 'none';
							});
						}
					});
				}
			});

			var map_id = $('#map_content');
			if (map_id.length > 0) {
				var $lat = map_id.data('lat'),
					$lng = map_id.data('lng'),
					$zoom = map_id.data('zoom'),
					$maptitle = map_id.data('maptitle'),
					$mapaddress = map_id.data('mapaddress'),
					mymap = L.map('map_content').setView([$lat, $lng], $zoom);

				L.tileLayer('http://tile.openstreetmap.org/{z}/{x}/{y}.png', {
					attribution: 'Map',
					maxZoom: 14,
					minZoom: 2,
					id: 'mapbox.streets',
					scrollWheelZoom: false 
				}).addTo(mymap);

				//var marker = L.marker([$lat, $lng]).addTo(mymap);
				mymap.zoomControl.setPosition('bottomright');
				mymap.scrollWheelZoom.disable();
			}

			//$("select").niceSelect();
			
		})();

	</script>

</body>
</html>